import Calculator from './components/Calculator/index.jsx'
import './App.scss'

function App() {
  return (
    <div className="App">
      <Calculator />
    </div>
  );
}

export default App;
