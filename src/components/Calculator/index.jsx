import React, { useState  } from "react";
import './index.scss'

function Calculator() {
  
  const [input1, setInput1] = useState(0)
  const [input2, setInput2] = useState(0)
  const [isAddition, setIsAddition] = useState(false)
  const [isSubtraction, setIsSubtraction] = useState(false)
  const [isMultiplication, setIsMultiplication] = useState(false)
  const [isDivision, setIsDivision] = useState(false)
  const [monitorValue, setMonitorValue] = useState('0')

  function numberClick(val) {
    if (!isOperatorClicked()) {
      if (monitorValue === '0') {
        if (val === '.') {
          let tempVal = monitorValue.concat(val)
          setMonitorValue(tempVal)
        } else {
          setValues(val)
        }
      } else {
        let tempVal = monitorValue.concat(val)
        setValues(tempVal)
      }
    } else if (isOperatorClicked()) {
      if (monitorValue === '0') {
        setValues(val)
        if (val === '.') {
          let tempVal = monitorValue.concat(val)
          console.log(tempVal)
          setMonitorValue(tempVal)
        }
      } else {
        let tempVal = monitorValue.concat(val)
        setValues(tempVal)
      }
    }
  }

  function setValues(val) {
    if (!isOperatorClicked()) {
      setMonitorValue(val)
      setInput1(parseFloat(val))
    }
    if (isOperatorClicked()) {
      setMonitorValue(val)
      setInput2(parseFloat(val))
    }
  }

  function isOperatorClicked() {
    return isAddition || isSubtraction || isMultiplication || isDivision
  }

  function clickOperator(operator) {
    if (input1 && input2) {
      getEqual()
    }
    if (operator === 'add') {
      setIsAddition(true)
    }
    if (operator === 'subtract') {
      setIsSubtraction(true)
    }
    if (operator === 'multiply') {
      setIsMultiplication(true)
    }
    if (operator === 'divide') {
      setIsDivision(true)
    }
    setMonitorValue('0')
  }

  function reset() {
    setIsAddition(false)
    setIsSubtraction(false)
    setIsMultiplication(false)
    setIsDivision(false)
    setInput2(0)
  }

  function getEqual() {
    let answer
    if (input1 && input2) {
      if (isAddition) {
        answer = input1 + input2
      }
      if (isSubtraction) {
        answer = input1 - input2
      }
      if (isMultiplication) {
        answer = input1 * input2
      }
      if (isDivision) {
        answer = input1 / input2
      }
      setMonitorValue(answer.toString())
      setInput1(answer)
      reset()
    }
    console.log('input1 ==> ', input1)
    console.log('input2 ==> ', input2)
    console.log('equal')
  }
  
  function clearValues() {
    if (input1 && !input2) {
      setInput1(0)
      setInput2(0)
      setMonitorValue('0')
    }
    if (input1 && input2) {
      setInput2(0)
      setMonitorValue('0')
    }
  }

  function getClearButtonName() {
    if (monitorValue != 0) {
      return 'C'
    }
    return 'AC'
  }

  function percent() {
    const fixedDigits = monitorValue.replace(/^-?\d*\.?/, '')
    const newVal = parseFloat(monitorValue) / 100
    setValues(newVal.toFixed(fixedDigits.length + 2).toString())   
  }

  function sign() {
    let newVal = parseFloat(monitorValue) * -1
    setValues(newVal.toString())
  }
  return (
    <div id="calculator">
      <div className="monitor">{ monitorValue }</div>
      <div className="flex-container">
          <button className="flex-item calculator-operator-secondary" onClick={clearValues}>{ getClearButtonName() }</button>
          <button className="flex-item calculator-operator-secondary" onClick={sign}>±</button>
          <button className="flex-item calculator-operator-secondary" onClick={percent}>%</button>
          <button className="flex-item calculator-operator-primary" onClick={() => clickOperator('divide')}>÷</button>
      </div>
      <div className="flex-container">
          <button className="flex-item" onClick={() => numberClick('7')}>7</button>
          <button className="flex-item" onClick={() => numberClick('8')}>8</button>
          <button className="flex-item" onClick={() => numberClick('9')}>9</button>
          <button className="flex-item calculator-operator-primary" onClick={() => clickOperator('multiply')}>×</button>
      </div>
      <div className="flex-container">
          <button className="flex-item" onClick={() => numberClick('4')}>4</button>
          <button className="flex-item" onClick={() => numberClick('5')}>5</button>
          <button className="flex-item" onClick={() => numberClick('6')}>6</button>
          <button className="flex-item calculator-operator-primary" onClick={() => clickOperator('subtract')}>−</button>
      </div>
      <div className="flex-container">
          <button className="flex-item" onClick={() => numberClick('1')}>1</button>
          <button className="flex-item" onClick={() => numberClick('2')}>2</button>
          <button className="flex-item" onClick={() => numberClick('3')}>3</button>
          <button className="flex-item calculator-operator-primary" onClick={() => clickOperator('add')}>+</button>
      </div>
      <div className="flex-container">
          <button className="flex-item width-160" onClick={() => numberClick('0')}>0</button>
          <button className="flex-item" onClick={() => numberClick('.')}>.</button>
          <button className="flex-item calculator-operator-primary" onClick={getEqual}>=</button>
      </div>
    </div>
  )
}
export default Calculator
